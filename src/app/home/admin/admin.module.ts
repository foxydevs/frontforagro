import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from "angular2-datatable";
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoadersCssModule } from 'angular2-loaders-css';
import { ChartsModule } from 'ng2-charts';
import { Ng2MapModule} from 'ng2-map';
import { Ng2DragDropModule } from 'ng2-drag-drop';

import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';

import { AdminRoutingModule } from './admin.routing';

import { UsersService } from "./_services/users.service";
import { EmployeesService } from "./_services/employees.service";
import { RolesService } from "./_services/roles.service";
import { PuestosService } from "./_services/puestos.service";
import { SucursalesService } from "./_services/sucursales.service";
import { ClientesService } from "./_services/clientes.service";
import { ProveedoresService } from "./_services/proveedores.service";
import { ModulosService } from "./_services/modulos.service";
import { AccesosService } from "./_services/accesos.service";
import { ComprasService } from "./_services/compras.service";
import { VentasService } from "./_services/ventas.service";
import { InventarioService } from "./_services/inventario.service";
import { ProductosService } from "./_services/productos.service";
import { TiposProductoService } from "./_services/tipos-producto.service";
import { TiposVentaService } from "./_services/tipos-venta.service";
import { TiposCompraService } from "./_services/tipos-compra.service";
import { CuentasCobrarService } from "./_services/cuentas-cobrar.service";
import { CuentasPagarService } from "./_services/cuentas-pagar.service";
import { MovimientosPagarService } from "./_services/movimientos-pagar.service";
import { MovimientosCobrarService } from "./_services/movimientos-cobrar.service";
import { GastosService } from "./_services/gastos.service";
import { SueldosService } from "./_services/sueldos.service";
import { ComisionesService } from "./_services/comisiones.service";
import { EstadisticasService } from "./_services/estadisticas.service";
import { CamionesService } from "./_services/camiones.service";
import { EmpleadosCamionService } from "./_services/empleados-camion.service";
import { InventarioCamionService } from "./_services/inventario-camion.service";
import { TipoAvanceService } from "./_services/tipo-avance.service";
import { TipoClienteService } from "./_services/tipo-cliente.service";
import { TipoVisitaService } from "./_services/tipo-visita.service";
import { UbicacionesService } from "./_services/ubicaciones.service";
import { ClientesUsuariosService } from "./_services/clientes-usuarios.service";
import { FormDiarioService } from "./_services/form-diario.service";
import { FormVisitaService } from "./_services/form-visita.service";
import { PaisesService } from "./_services/paises.service";
import { SectoresService } from "./_services/sectores.service";

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ComprasComponent } from './compras/compras.component';
import { VentasComponent } from './ventas/ventas.component';
import { InventarioComponent } from './inventario/inventario.component';
import { PagosComponent } from './pagos/pagos.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { LoaderComponent } from './loader/loader.component';
import { RolesComponent } from './roles/roles.component';
import { PuestosComponent } from './puestos/puestos.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { PerfilComponent } from './perfil/perfil.component';
import { GenerarCompraComponent } from './generar-compra/generar-compra.component';
import { GenerarVentaComponent } from './generar-venta/generar-venta.component';
import { VentasAnuladasComponent } from './ventas-anuladas/ventas-anuladas.component';
import { ComprasAnuladasComponent } from './compras-anuladas/compras-anuladas.component';
import { CotizacionComponent } from './cotizacion/cotizacion.component';
import { ModulosComponent } from './modulos/modulos.component';
import { TiposProductoComponent } from './tipos-producto/tipos-producto.component';
import { CuentasCobrarComponent } from './cuentas-cobrar/cuentas-cobrar.component';
import { CuentasPagarComponent } from './cuentas-pagar/cuentas-pagar.component';
import { CuentasCobrarPagadasComponent } from './cuentas-cobrar-pagadas/cuentas-cobrar-pagadas.component';
import { CuentasPagarPagadasComponent } from './cuentas-pagar-pagadas/cuentas-pagar-pagadas.component';
import { InventarioAdminComponent } from './inventario-admin/inventario-admin.component';
import { InventarioInicialComponent } from './inventario-inicial/inventario-inicial.component';
import { SueldosComponent } from './sueldos/sueldos.component';
import { ComisionesComponent } from './comisiones/comisiones.component';
import { EstadisticaVendedorComponent } from './estadistica-vendedor/estadistica-vendedor.component';
import { EstadisticaFlujoComponent } from './estadistica-flujo/estadistica-flujo.component';
import { EstadisticaVentasComponent } from './estadistica-ventas/estadistica-ventas.component';
import { EstadisticaClientesComponent } from './estadistica-clientes/estadistica-clientes.component';
import { DiarioComponent } from './conta/diario/diario.component';
import { MayorComponent } from './conta/mayor/mayor.component';
import { BalanceComponent } from './conta/balance/balance.component';
import { EstadoResultadosComponent } from './conta/estado-resultados/estado-resultados.component';
import { FlujoComponent } from './conta/flujo/flujo.component';
import { BalanceSaldosComponent } from './conta/balance-saldos/balance-saldos.component';
import { TipoVisitaComponent } from './tipo-visita/tipo-visita.component';
import { TipoClienteComponent } from './tipo-cliente/tipo-cliente.component';
import { TipoAvanceComponent } from './tipo-avance/tipo-avance.component';
import { CamionesComponent } from './camiones/camiones.component';
import { InventarioCamionComponent } from './inventario-camion/inventario-camion.component';
import { EmpleadosCamionComponent } from './empleados-camion/empleados-camion.component';
import { ClientesUsuariosComponent } from './clientes-usuarios/clientes-usuarios.component';
import { ObjetivosComponent } from './objetivos/objetivos.component';
import { CultivosComponent } from './cultivos/cultivos.component';
import { PaisesComponent } from './paises/paises.component';
import { SectoresComponent } from './sectores/sectores.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCdJAwErIy3KmcE_EfHACIvL0Nl1RjhcUo'}),
    Ng2SearchPipeModule,
    Ng2DragDropModule.forRoot(),
    LoadersCssModule,
    AngularMultiSelectModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    DashboardComponent,
    UsuariosComponent,
    ComprasComponent,
    VentasComponent,
    InventarioComponent,
    PagosComponent,
    ClientesComponent,
    ProveedoresComponent,
    EmpleadosComponent,
    LoaderComponent,
    RolesComponent,
    PuestosComponent,
    SucursalesComponent,
    PerfilComponent,
    GenerarCompraComponent,
    GenerarVentaComponent,
    VentasAnuladasComponent,
    ComprasAnuladasComponent,
    CotizacionComponent,
    ModulosComponent,
    TiposProductoComponent,
    CuentasCobrarComponent,
    CuentasPagarComponent,
    CuentasCobrarPagadasComponent,
    CuentasPagarPagadasComponent,
    InventarioAdminComponent,
    InventarioInicialComponent,
    SueldosComponent,
    ComisionesComponent,
    EstadisticaVendedorComponent,
    EstadisticaFlujoComponent,
    EstadisticaVentasComponent,
    EstadisticaClientesComponent,
    DiarioComponent,
    MayorComponent,
    BalanceComponent,
    EstadoResultadosComponent,
    FlujoComponent,
    BalanceSaldosComponent,
    TipoVisitaComponent,
    TipoClienteComponent,
    TipoAvanceComponent,
    CamionesComponent,
    InventarioCamionComponent,
    EmpleadosCamionComponent,
    ClientesUsuariosComponent,
    ObjetivosComponent,
    CultivosComponent,
    PaisesComponent,
    SectoresComponent
  ],
  providers: [
    UsersService,
    EmployeesService,
    RolesService,
    SucursalesService,
    PuestosService,
    ClientesService,
    ProveedoresService,
    ModulosService,
    AccesosService,
    ComprasService,
    VentasService,
    InventarioService,
    ProductosService,
    TiposProductoService,
    TiposVentaService,
    TiposCompraService,
    CuentasCobrarService,
    CuentasPagarService,
    MovimientosPagarService,
    MovimientosCobrarService,
    GastosService,
    SueldosService,
    FormDiarioService,
    ComisionesService,
    EstadisticasService,
    CamionesService,
    EmpleadosCamionService ,
    InventarioCamionService,
    TipoAvanceService,
    TipoClienteService,
    UbicacionesService,
    TipoVisitaService,
    FormVisitaService,
    ClientesUsuariosService,
    PaisesService,
    SectoresService
  ]
})
export class AdminModule { }
