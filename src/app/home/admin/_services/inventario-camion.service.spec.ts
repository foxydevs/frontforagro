import { TestBed, inject } from '@angular/core/testing';

import { InventarioCamionService } from './inventario-camion.service';

describe('InventarioCamionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InventarioCamionService]
    });
  });

  it('should be created', inject([InventarioCamionService], (service: InventarioCamionService) => {
    expect(service).toBeTruthy();
  }));
});
