import { TestBed, inject } from '@angular/core/testing';

import { FormVisitaService } from './form-visita.service';

describe('FormVisitaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormVisitaService]
    });
  });

  it('should be created', inject([FormVisitaService], (service: FormVisitaService) => {
    expect(service).toBeTruthy();
  }));
});
