import { TestBed, inject } from '@angular/core/testing';

import { EmpleadosCamionService } from './empleados-camion.service';

describe('EmpleadosCamionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmpleadosCamionService]
    });
  });

  it('should be created', inject([EmpleadosCamionService], (service: EmpleadosCamionService) => {
    expect(service).toBeTruthy();
  }));
});
