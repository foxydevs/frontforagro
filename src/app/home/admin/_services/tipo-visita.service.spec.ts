import { TestBed, inject } from '@angular/core/testing';

import { TipoVisitaService } from './tipo-visita.service';

describe('TipoVisitaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoVisitaService]
    });
  });

  it('should be created', inject([TipoVisitaService], (service: TipoVisitaService) => {
    expect(service).toBeTruthy();
  }));
});
