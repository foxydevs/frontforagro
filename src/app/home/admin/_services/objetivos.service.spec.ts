import { TestBed, inject } from '@angular/core/testing';

import { ObjetivosService } from './objetivos.service';

describe('ObjetivosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObjetivosService]
    });
  });

  it('should be created', inject([ObjetivosService], (service: ObjetivosService) => {
    expect(service).toBeTruthy();
  }));
});
