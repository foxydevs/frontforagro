import { TestBed, inject } from '@angular/core/testing';

import { CultivosService } from './cultivos.service';

describe('CultivosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CultivosService]
    });
  });

  it('should be created', inject([CultivosService], (service: CultivosService) => {
    expect(service).toBeTruthy();
  }));
});
