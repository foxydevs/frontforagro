import { TestBed, inject } from '@angular/core/testing';

import { ClientesUsuariosService } from './clientes-usuarios.service';

describe('ClientesUsuariosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientesUsuariosService]
    });
  });

  it('should be created', inject([ClientesUsuariosService], (service: ClientesUsuariosService) => {
    expect(service).toBeTruthy();
  }));
});
