import { TestBed, inject } from '@angular/core/testing';

import { FormDiarioService } from './form-diario.service';

describe('FormDiarioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormDiarioService]
    });
  });

  it('should be created', inject([FormDiarioService], (service: FormDiarioService) => {
    expect(service).toBeTruthy();
  }));
});
