import { TestBed, inject } from '@angular/core/testing';

import { TipoAvanceService } from './tipo-avance.service';

describe('TipoAvanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoAvanceService]
    });
  });

  it('should be created', inject([TipoAvanceService], (service: TipoAvanceService) => {
    expect(service).toBeTruthy();
  }));
});
