import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoAvanceComponent } from './tipo-avance.component';

describe('TipoAvanceComponent', () => {
  let component: TipoAvanceComponent;
  let fixture: ComponentFixture<TipoAvanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoAvanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoAvanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
