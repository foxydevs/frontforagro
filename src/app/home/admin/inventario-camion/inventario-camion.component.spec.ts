import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventarioCamionComponent } from './inventario-camion.component';

describe('InventarioCamionComponent', () => {
  let component: InventarioCamionComponent;
  let fixture: ComponentFixture<InventarioCamionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventarioCamionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventarioCamionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
