import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadosCamionComponent } from './empleados-camion.component';

describe('EmpleadosCamionComponent', () => {
  let component: EmpleadosCamionComponent;
  let fixture: ComponentFixture<EmpleadosCamionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadosCamionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadosCamionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
