import { Component, OnInit } from '@angular/core';
import { UsersService } from "./../_services/users.service";
import { EstadisticasService } from "./../_services/estadisticas.service";
import { UbicacionesService } from "./../_services/ubicaciones.service";
import { ClientesService } from "./../_services/clientes.service";
import { FormDiarioService } from "./../_services/form-diario.service";
import { FormVisitaService } from "./../_services/form-visita.service";

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title:string="Dashboard"
  Table:any
  idRol=+localStorage.getItem('currentRolId');
  Agregar = +localStorage.getItem('permisoAgregar')
  Modificar = +localStorage.getItem('permisoModificar')
  Eliminar = +localStorage.getItem('permisoEliminar')
  Mostrar = +localStorage.getItem('permisoMostrar')
  selectedData:any
  comboUsuarios:any
  usuarios:any = 'All';
  paisMain:any = 'All';
  sectorMain:any = 'All';
  public rowsOnPage = 5;
  public search:any
  fechaHoy:any
  fechaMes:any
  posis:string = "http://foxylabs.xyz/Documentos/imgs/agent2.png"
  posis2:string = "http://foxylabs.xyz/Documentos/imgs/customer2.png"
  posis3:string = "http://foxylabs.xyz/Documentos/imgs/delivery-truck2.png"
  posis4:string = "http://foxylabs.xyz/Documentos/imgs/form2.png"
  positions:any = []
  positions2:any = []
  positions3:any = []
  positions4:any = []
  locate:number = 0;
  locate2:number = 1;
  locate3:number = 0;
  locate4:number = 0;
  lat:any = 14.66530813990437
  lng:any = -90.51709514672852
  constructor(
    private _service: NotificationsService,
    private mainService: EstadisticasService,
    private fourthService: FormDiarioService,
    private secondService: UbicacionesService,
    private thirthService: ClientesService,
    private fiveService: FormVisitaService,
    private secondParentService: UsersService
  ) { }
  public pieChartLabels = null;
  public pieChartData = null;
  public pieChartType:string = 'pie';


  ngOnInit() {
    this.cargarLogradosDiarios();
    setTimeout(() => {
      this.getDate();
      this.cargarAll();
      this.cargarSecondParentCombo();
    }, 500);

    // this.ubicar();
  }



  cargarSecondParentCombo(){
    this.secondParentService.getAll()
                      .then(response => {
                        this.comboUsuarios = response
                      // console.log(response);
                      console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  onMapReady(map) {

  }
  onIdle(event) {
    // console.log('map', event.target);
  }
  onMarkerInit(marker) {
    // console.log('marker', marker);
  }
  onMapClick(event) {
    this.positions.push(event.latLng);
    let positions1 = event.latLng + '';
    let pos = positions1.replace(')','').replace('(','').split(',')
    this.lat = pos[0].replace(' ','')
    this.lng = pos[1].replace(' ','')

    // event.target.panTo(this.positions);
    // this.positions2.push(new google.maps.LatLng(parseFloat(this.lat)+0.01032, parseFloat(this.lng)+0.01032));
    // this.positions3.push(new google.maps.LatLng(parseFloat(this.lat)+0.02055, parseFloat(this.lng)+0.01055));
    // this.positions4.push(new google.maps.LatLng(parseFloat(this.lat)+0.03095, parseFloat(this.lng)+0.01095));
    // this.positions = new google.maps.LatLng(parseFloat(response.latitude), parseFloat(response.longitude));
    // console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+this.positions);
  }
  ubicar(){
    if (navigator.geolocation) {
      let pos
      navigator.geolocation.getCurrentPosition(function(position) {
        pos = position.coords.latitude
      });
      // console.log(pos+' posicion');
    } else {
      // Browser doesn't support Geolocation
    }
  }
  clicked11(data:any){
    // console.log(data);


  }
  cargarAll(){

    let data1 = [this.locate,this.locate2,this.locate3,this.locate4]
    this.locate = 0;
    this.locate2 = 0;
    this.locate3 = 0;
    this.locate4 = 0;
    if(this.usuarios=="All"){
        $(document).ready(function(){
          $('#Loading').css('display','block')
          $('#Loading').addClass('in')
        });
        this.secondService.getAll()
                          .then(response => {
                            this.selectedData = response;
                            this.positions.length = 0;
                            response.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitude), parseFloat(element.longitude));
                              this.positions.push(pos)
                            });
                            $('#Loading').css('display','none')
                            // console.log(this.positions);
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })
        let data = {
          "latitude" : this.lat,
          "longitude" : this.lng
        }
        this.thirthService.getAllNear(data)
                          .then(response2 => {
                            this.selectedData = response2;
                            this.positions2.length = 0;
                            response2.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitud), parseFloat(element.longitud));
                              this.positions2.push(pos)
                            });
                            // console.log(this.positions);
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })
        this.fourthService.getAll()
                          .then(response4 => {
                            this.selectedData = response4;
                            this.positions4.length = 0;
                            response4.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitud), parseFloat(element.longitud));
                              this.positions4.push(pos)
                            });
                            // console.log(this.positions4+' esto '+response4);
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })
        this.fiveService.getAll()
                          .then(response4 => {
                            this.selectedData = response4;
                            this.positions4.length = 0;
                            response4.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitud), parseFloat(element.longitud));
                              this.positions4.push(pos)
                            });
                            // console.log(this.positions4+' esto '+response4);
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })
    }else{
      $(document).ready(function(){
        $('#Loading').css('display','block')
        $('#Loading').addClass('in')
      });
      this.secondService.getAllMine(+this.usuarios)
                          .then(response => {
                            this.selectedData = response;
                            this.positions.length = 0;
                            response.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitude), parseFloat(element.longitude));
                              this.positions.push(pos)
                            });
                            // console.log(this.positions);
                            $('#Loading').css('display','none')
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })
        let data = {
          "latitude" : this.lat,
          "longitude" : this.lng
        }
        this.thirthService.getAllMine(+this.usuarios,data)
                          .then(response2 => {
                            this.selectedData = response2;
                            this.positions2.length = 0;
                            response2.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitud), parseFloat(element.longitud));
                              this.positions2.push(pos)
                            });
                            // console.log(this.positions);
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })
        this.fourthService.getAllMine(+this.usuarios)
                          .then(response4 => {
                            this.selectedData = response4;
                            this.positions4.length = 0;
                            response4.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitud), parseFloat(element.longitud));
                              this.positions4.push(pos)
                            });
                            // console.log(this.positions4+' esto '+response4);
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })
        this.fiveService.getAllMine(+this.usuarios)
                          .then(response4 => {
                            this.selectedData = response4;
                            this.positions4.length = 0;
                            response4.forEach(element => {
                              let pos = new google.maps.LatLng(parseFloat(element.latitud), parseFloat(element.longitud));
                              this.positions4.push(pos)
                            });
                            // console.log(this.positions4+' esto '+response4);
                          }).catch(error => {
                            console.clear
                            this.createError(error)
                            $('#Loading').css('display','none')
                          })

    }
    this.locate = data1[0];
    this.locate2 = data1[1];
    this.locate3 = data1[2];
    this.locate4 = data1[3];
    setTimeout(function(){
    },1100)

  }

  getDate(){
    let date = new Date();
    let month = date.getMonth()+1;
    let month2;
    let diaB = date.getDate()-1;
    let diaB2;
    let dia= date.getDate();
    let dia2;
    if(month<10){
      month2='0'+month;
    }else{
      month2=month
    }
    if(diaB<10){
      diaB2='0'+diaB;
    }else{
      diaB2=diaB
    }
    if(dia<10){
      dia2='0'+dia;
    }else{
      dia2=dia
    }
    this.fechaHoy= date.getFullYear()+'-'+month2+'-'+dia2
    this.fechaMes= date.getFullYear()+'-'+month2+'-'+diaB2
  }

  cargarLogradosDiarios(){
    this.pieChartData = null
    this.pieChartLabels = null
    if(this.usuarios=="All"){
      this.fourthService.getLogrados()
                        .then(response => {
                          let etiqueta = ""
                          let labels:any[] = []
                          let data:any[] = []


                          response.forEach(element => {
                            etiqueta = ((element.logrado!=null)?((element.logrado==0)?'No Logrados':'Logrado'):'No Especificado');
                            labels.push(etiqueta)
                            data.push(element.cantidad)
                          });
                          this.pieChartLabels=labels
                          this.pieChartData=data
                        // console.log(this.pieChartLabels);
                        // console.log(this.pieChartData);
                        // console.log(response);
                        console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }else{
      this.fourthService.getLogradosMine(+this.usuarios)
                        .then(response => {
                          let etiqueta = ""
                          let labels:any[] = []
                          let data:any[] = []


                          response.forEach(element => {
                            etiqueta = ((element.logrado!=null)?((element.logrado==0)?'No Logrados':'Logrado'):'No Especificado');
                            labels.push(etiqueta)
                            data.push(element.cantidad)
                          });
                          this.pieChartLabels=labels
                          this.pieChartData=data
                        console.log(this.pieChartLabels);
                        console.log(this.pieChartData);
                        console.log(response);
                        console.clear
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                        })
    }
  }
  cargarPie(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    let data = {
      fechaInicio: this.fechaMes,
      fechaFin: this.fechaHoy
    }
    this.pieChartData = null
    this.pieChartLabels = null
    this.mainService.getPieVentas(data)
                      .then(response => {
                          let labels:any[] = []
                          let data:any[] = []
                          response.forEach((element,index) => {
                            labels.push(element.nombre)
                            data.push(element.total)

                          });
                          // if(fortex.length>0 && fortexData.length>0){
                          //   this.pieChartLabels = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
                          //   this.pieChartData = [300, 500, 100];
                          // }else{
                          //   this.pieChartData = [0]
                          //   this.pieChartLabels = ["Vacio"]
                          // }
                          this.pieChartLabels=labels
                          this.pieChartData=data
                        // console.log(this.pieChartLabels);
                        // console.log(this.pieChartData)

                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }

  public chartClicked(e:any):void {
    // console.log(e);
  }

  public chartHovered(e:any):void {
    // console.log(e);
  }

  public options = {
    position: ["bottom", "right"],
    timeOut: 2000,
    lastOnBottom: false,
    animate: "fromLeft",
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
};

create(success) {
     this._service.success('¡Éxito!',success)

}
createError(error) {
     this._service.error('¡Error!',error)

}
}
