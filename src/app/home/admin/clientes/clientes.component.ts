import { Component, OnInit } from '@angular/core';
import { ClientesService } from "./../_services/clientes.service";

import { NotificationsService } from 'angular2-notifications';

declare var $: any

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  title:string="Clientes"
  Table:any
  selectedData:any
  idRol=+localStorage.getItem('currentRolId');
  Agregar = +localStorage.getItem('permisoAgregar')
  Modificar = +localStorage.getItem('permisoModificar')
  Eliminar = +localStorage.getItem('permisoEliminar')
  Mostrar = +localStorage.getItem('permisoMostrar')
  public rowsOnPage = 5;
  public search:any
  positions:any
  lat:any = 14.66530813990437
  lng:any = -90.51709514672852
  constructor(
    private _service: NotificationsService,
    private mainService: ClientesService
  ) { }

  ngOnInit() {
    this.cargarAll()
  }
  onMapReady(map) {

  }
  onIdle(event) {
    // console.log('map', event.target);
  }
  onMarkerInit(marker) {
    // console.log('marker', marker);
  }
  onMapClick(event) {
    this.positions = event.latLng;
    let positions1 = event.latLng + '';
    let pos = positions1.replace(')','').replace('(','').split(',')
    this.lat = pos[0].replace(' ','')
    this.lng = pos[1].replace(' ','')
    // event.target.panTo(this.positions);
    this.positions = new google.maps.LatLng(parseFloat(this.lat), parseFloat(this.lng));
    // this.positions = new google.maps.LatLng(parseFloat(response.latitude), parseFloat(response.longitude));
    console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+this.positions);
  }
  ubicar(){
    if (navigator.geolocation) {
      let pos
      navigator.geolocation.getCurrentPosition(function(position) {
        pos = position.coords.latitude
      });
      console.log(pos+' posicion');
    } else {
      // Browser doesn't support Geolocation
    }
  }
  clicked11(data:any){
    console.log(data);


  }
  cargarAll(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getAll()
                      .then(response => {
                        this.Table = response


                         // console.log(response);
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.create(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Cliente Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form')[0].reset()
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })


  }

  cargarSingle(id:number){
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        this.positions = new google.maps.LatLng(parseFloat((response.latitud+'')), parseFloat((response.longitud+'')));
                        // this.positions = "("+response.latitud+", "+response.longitud+")";
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue.latitud = this.lat;
    formValue.longitud = this.lng;
    console.log(formValue)
    this.mainService.update(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Cliente Actualizado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })

  }

  delete(id:string){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    if(confirm("¿Desea eliminar el Cliente?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Cliente Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }else{
      $('#Loading').css('display','none')
    }

  }

  public options = {
    position: ["bottom", "right"],
    timeOut: 2000,
    lastOnBottom: false,
    animate: "fromLeft",
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
};

create(success) {
     this._service.success('¡Éxito!',success)

}
createError(error) {
     this._service.error('¡Error!',error)

}
}
