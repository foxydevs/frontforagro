import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesUsuariosComponent } from './clientes-usuarios.component';

describe('ClientesUsuariosComponent', () => {
  let component: ClientesUsuariosComponent;
  let fixture: ComponentFixture<ClientesUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
